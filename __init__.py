# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# epository contains the full copyright notices and license terms.
from trytond.pool import Pool
import party
import account
import invoice
import sale
import invoice_authorization


def register():
    Pool.register(
        invoice_authorization.InvoiceAuthorization,
        account.Move,
        account.Reconciliation,
        invoice.Invoice,
        sale.Sale,
        party.Party,
        module='account_dom', type_='model')
