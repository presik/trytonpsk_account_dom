# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval

__all__ = ['InvoiceAuthorization']


STATES = {
    'readonly': Eval('state') != 'draft',
}

KIND = [
    ('', ''),
    ('C', 'Computador'),
    ('P', 'POS'),
    ('M', 'Manual'),
    ('1', 'Venta Electronica'),
    ('2', 'Exportacion'),
    ('3', 'Factura por Contingencia Facturador'),
    ('4', 'Factura por Contingencia DIAN'),
    ('91', 'Nota Crédito Eléctronica'),
    ('92', 'Nota Débito Eléctronica'),
]

class InvoiceAuthorization(ModelSQL, ModelView):
    'Invoice Authorization'
    __name__ = 'account.invoice.authorization'
    _rec_name = 'number'
    number = fields.Char('Number Authorization', required=True, states=STATES,)

    software_provider_id = fields.Char('Software Provider Id',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
            'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
        })
    start_date_auth = fields.Date('Start Date Auth', required=True,
        states=STATES)
    end_date_auth = fields.Date('End Date Auth', required=True, states=STATES)
    from_auth = fields.Integer('From Auth', required=True, states=STATES)
    to_auth = fields.Integer('To Auth', required=True, states=STATES)
    sequence = fields.Many2One('ir.sequence.strict', 'Sequence', required=True,
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES)
    kind = fields.Selection(KIND, 'Kind', required=True, states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'State', select=True)

    @classmethod
    def __setup__(cls):
        super(InvoiceAuthorization, cls).__setup__()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'
